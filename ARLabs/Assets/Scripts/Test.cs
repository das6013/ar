using UnityEngine;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARTrackedImageManager))]
public class Test : MonoBehaviour
{
    private ARTrackedImageManager _trackedImageManager;

    private void Awake()
    {
        _trackedImageManager=GetComponent<ARTrackedImageManager>();
    }

    void OnEnable()
    {
        _trackedImageManager.trackedImagesChanged += OnChanged;
    }

    void OnDisable()
    {
        _trackedImageManager.trackedImagesChanged -= OnChanged;
    }

    void OnChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var newImage in eventArgs.added)
        {
           Debug.Log("ADD");
        }

        foreach (var updatedImage in eventArgs.updated)
        {
            Debug.Log("Update");
        }

        foreach (var removedImage in eventArgs.removed)
        {
            Debug.Log("Update");
        }
    }
}
